# NCBI ASSEMBLIES INFO

Retrieve assemblies for a taxon from NCBI and generate a list of number of assemblies per order.

## Setup

Required modules are in `requirements.txt`

Recommended setup (requires virtualenv):

```bash
git clone git@gitlab.com:evogenlab/ncbi_assemblies_info.git
cd ncbi_assemblies_info
virtualenv venv
source venv/bin/activate
pip install requirements.txt
```

## Usage

Important settings are detailed at the top of the main file.

Results are output to stdout, logs to stderr.

Just run 
```bash
python3 ncbi_assemblies_info.py > assemblies_info.tsv
```
