import datetime
import logging
import pathlib
import time
from collections import defaultdict
from urllib.error import HTTPError
from Bio import Entrez
from ete3 import NCBITaxa

# Base taxon
TAXON = 'Arthropoda'

# If you have an Entrez account, fill in your API key and email to increase
# requests per second from 3 to 10
ENTREZ_API_KEY = None
ENTREZ_EMAIL = None

ENTREZ_API_KEY = '9b747e0153782a735ef80e1ded2e7618be08'
ENTREZ_EMAIL = 'romain.feron@unil.ch'

# NCBI Entrez connection parameters
ENTREZ_MAX_TRIES = 5
ENTREZ_SLEEP_BETWEEN_TRIES = 10


# Setup logging
logging.basicConfig(level=logging.INFO,
                    format='[%(asctime)s]::%(levelname)s  %(message)s',
                    datefmt='%Y.%m.%d - %H:%M:%S')


def query_wrapper(tool, max_tries=10, sleep_between_tries=5, **args):
    '''
    Small wrapper function to query NCBI.
    Handles all Entrez tools, arguments are gotten from **args.
    If the query fails, will attempt again every <sleep_between_tries> seconds
    until <max_tries>.
    '''
    retry_count = 0
    while True:
        retry_count += 1
        logging.info(f'Querying <{tool}>: attempt {retry_count}/{max_tries}')
        try:
            handle = getattr(Entrez, tool)(**args)
            record = Entrez.read(handle, validate=False)
            logging.info(f'Querying <{tool}> was successful on attempt {retry_count}/{max_tries}')
            break
        except (HTTPError, RuntimeError) as error:
            logging.error(f'Querying <{tool}> failed on attempt {retry_count}/{max_tries}: <{error}>')
            if 'Empty result' in str(error):
                logging.error(f'No assembly found for this query.')
                exit(1)
            if retry_count == max_tries:
                logging.error(f'Reached max attempts, cannot connect to NCBI.')
                exit(1)
        time.sleep(sleep_between_tries)
    return record


# Main script execution
if __name__ == '__main__':

    Entrez.api_key = ENTREZ_API_KEY
    Entrez.email = ENTREZ_EMAIL
    max_tries = ENTREZ_MAX_TRIES
    sleep_between_tries = ENTREZ_SLEEP_BETWEEN_TRIES

    query = f'"{TAXON}"[Organism] AND latest[filter] AND all[filter] NOT anomalous[filter] AND all[filter] NOT partial[filter]'

    # First query (esearch): get list of all assemblies
    record = query_wrapper('esearch', max_tries, sleep_between_tries,
                           db="assembly", term=query, usehistory='y')
    assembly_count = record['Count']
    query_key = record['QueryKey']
    web_env = record['WebEnv']

    # Second query (efetch): fetch all assembly data from WebEnv
    accession_list = query_wrapper('efetch', max_tries, sleep_between_tries,
                                   db="assembly", query_key=query_key, WebEnv=web_env)

    # Third query (esummary): retrieve all assembly data from WebEnv
    assemblies_info = query_wrapper('esummary', max_tries, sleep_between_tries,
                                    db="assembly", query_key=query_key, WebEnv=web_env)
    assemblies_info = assemblies_info['DocumentSummarySet']['DocumentSummary']

    # Extracting taxa IDs
    taxids = {int(assembly['Taxid']) for assembly in assemblies_info}
    logging.info(f'Retrieved {len(assemblies_info)} assemblies for {len(taxids)} species')

    # Initialize NCBI taxonomy DB from ete3. Update db if older than 1 day
    ncbi = NCBITaxa()
    update_db = False
    fname = pathlib.Path('taxdump.tar.gz')
    if not fname.exists():
        update_db = True
    else:
        modif_time = datetime.datetime.fromtimestamp(fname.stat().st_mtime)
        current_time = datetime.datetime.now()
        difference = current_time - modif_time
        if difference.days > 0:
            update_db = True
    if update_db:
        ncbi.update_taxonomy_database()

    orders = defaultdict(int)
    for taxid in taxids:
        lineage = ncbi.get_lineage(taxid)
        ranks = ncbi.get_rank(lineage)
        order = tuple(s for s, r in ranks.items() if r == 'order')
        if len(order) != 1:
            logging.warning(f'Could not find order for species taxid {taxid}')
            orders['NA'] += 1
            continue
        orders[order[0]] += 1

    order_names = ncbi.get_taxid_translator([o for o in orders.keys() if o != 'NA'])

    for order, count in orders.items():
        name = 'NA' if order == 'NA' else order_names[order]
        print(f'{name}\t{count}')
